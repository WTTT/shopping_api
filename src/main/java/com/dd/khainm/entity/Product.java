package com.dd.khainm.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="products")
@AttributeOverride(name = "id", column = @Column(name = "id", nullable = false, columnDefinition = "INT", unique = true))
public class Product extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	@Column(name="code", length = 20, nullable = false, unique = true)
	private String code;
	
	@Column(name ="image", length = Integer.MAX_VALUE, nullable = true)
	private byte[] image;
	
	@Column(name = "name", length = 255, nullable = false)
	private String name;
	
	@Column(name = "price", nullable = false)
	private double price;
	
	public Product() {
		super();
	}

	public Product(int id, String code, byte[] image, String name, double price, Date createdAt, Date deletedAt, Date updatedAt, boolean isDeleted) {
		super(id, createdAt, deletedAt, updatedAt, isDeleted);
		this.code = code;
		this.image = image;
		this.name = name;
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
