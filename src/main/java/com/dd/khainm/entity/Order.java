package com.dd.khainm.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "orders", uniqueConstraints = @UniqueConstraint(columnNames = {"order_num"}))
@AttributeOverride(name ="id", column = @Column(name="id", unique = true, nullable = false, columnDefinition = "int"))
public class Order extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "amount", nullable = false)
	private double amount;
	
	@Column(name = "customer_address", nullable = false, length = 255)
	private String customerAddress;
	
	@Column(name = "customer_email", nullable = false, length = 128)
	private String customerEmail;
	
	@Column(name = "customer_name", nullable = false, length = 255)
	private String customerName;
	
	@Column(name = "customer_phone", nullable = false, length = 128)
	private String customerPhone;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "order_date", nullable = false)
	private Date orderDate;
	
	@Column(name = "order_num", nullable = false)
	private int orderNum;
	
	public Order() {
		super();
	}

	public Order(int id, Date createdAt, Date deletedAt, Date updatedAt, boolean isDeleted, double amount, String customerAddress, String customerEmail, String customerName, String customerPhone,
			Date orderDate, int orderNum) {
		super(id, createdAt, deletedAt, updatedAt, isDeleted);
		this.amount = amount;
		this.customerAddress = customerAddress;
		this.customerEmail = customerEmail;
		this.customerName = customerName;
		this.customerPhone = customerPhone;
		this.orderDate = orderDate;
		this.orderNum = orderNum;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orederDate) {
		this.orderDate = orederDate;
	}

	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
