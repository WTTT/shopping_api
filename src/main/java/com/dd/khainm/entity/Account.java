package com.dd.khainm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "accounts", uniqueConstraints = @UniqueConstraint( columnNames = {"user_Name"} ))
public class Account extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "user_Name", nullable = false, length = 20)
	private String userName;
	
	@Column(name = "active", nullable = false)
	private boolean active;
	
	@Column(name = "encryted_password", nullable = false, length = 128)
	private String encrytedPassword;
	
	@Column(name = "user_role", nullable = false, length = 20)
	private String userRole;
	
	public Account() {
		super();
	}

	public Account(int id, Date createdAt, Date deletedAt, Date updatedAt, boolean isDeleted, String userName, boolean active, String encrytedPassword, String userRole) {
		super(id, createdAt, deletedAt, updatedAt, isDeleted);
		this.userName = userName;
		this.active = active;
		this.encrytedPassword = encrytedPassword;
		this.userRole = userRole;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getEncrytedPassword() {
		return encrytedPassword;
	}

	public void setEncrytedPassword(String encrytedPassword) {
		this.encrytedPassword = encrytedPassword;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
