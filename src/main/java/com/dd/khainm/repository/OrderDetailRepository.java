package com.dd.khainm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dd.khainm.entity.OrderDetail;

@Repository
public interface OrderDetailRepository extends BaseRepository<OrderDetail, Long> {
	
	@Override
	@Query(value = "SELECT t.* FROM order_details t WHERE t.id = :id ", nativeQuery = true)
	List<OrderDetail> findById(@Param("id") int id);
	
}
