package com.dd.khainm.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import com.dd.khainm.entity.BaseEntity;

@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity, ID extends Serializable> extends JpaRepository<T, ID>{
	@Query(value = "SELECT t.* FROM TABLE_NAME t WHERE t.id = :id ", nativeQuery = true)
	List<T> findById(@Param("id") int id);
}
