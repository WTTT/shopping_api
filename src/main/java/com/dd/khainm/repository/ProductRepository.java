package com.dd.khainm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dd.khainm.entity.Product;

@Repository
public interface ProductRepository extends BaseRepository<Product, Long> {
	@Override
	@Query(value = "SELECT t.* FROM Products t WHERE t.id = :id ", nativeQuery = true)
	List<Product> findById(@Param("id") int id);
	
	@Query(value = "SELECT t.* FROM Products t WHERE t.code = :code ", nativeQuery = true)
	List<Product> findById(@Param("code") String code);
}
