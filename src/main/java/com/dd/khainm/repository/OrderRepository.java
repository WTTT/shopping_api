package com.dd.khainm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dd.khainm.entity.Order;

@Repository
public interface OrderRepository extends BaseRepository<Order, Long>{
	
	@Override
	@Query(value = "SELECT t.* FROM orders t WHERE t.id = :id ", nativeQuery = true)
	List<Order> findById(@Param("id") int id);
}
