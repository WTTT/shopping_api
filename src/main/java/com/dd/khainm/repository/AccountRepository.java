package com.dd.khainm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dd.khainm.entity.Account;

@Repository
public interface AccountRepository extends BaseRepository<Account, Long>{
	
	@Override
	@Query(value = "SELECT t.* FROM accounts t WHERE t.id = :id ", nativeQuery = true)
	List<Account> findById(@Param("id") int id);
	
	@Query(value = "SELECT t.* FROM accounts t WHERE t.user_name = :name ", nativeQuery = true)
	List<Account> findById(@Param("name") String name);
}
