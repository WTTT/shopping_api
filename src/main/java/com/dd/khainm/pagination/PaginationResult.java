package com.dd.khainm.pagination;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.query.Query;

public class PaginationResult<E> {

	private int totalRecords;
	private int currentPage;
	private List<E> list;
	private int maxResult;
	private int totalPages;

	private int maxNavigationPage;

	private List<Integer> navigationPages;

	// @page: 1, 2, ..
	public PaginationResult(TypedQuery<E> query, int page, int maxResult, int maxNavigationPage) {
		final int pageIndex = page - 1 < 0 ? 0 : page - 1;

		int fromRecordIndex = pageIndex * maxResult;
		int maxRecordIndex = fromRecordIndex + maxResult;

		ScrollableResults resultScroll = ((Query<E>) query).scroll(ScrollMode.SCROLL_INSENSITIVE);

		List<E> results = new ArrayList<>();

		boolean hasResult = resultScroll.first();

		if (hasResult) {
			// Scroll to
			hasResult = resultScroll.scroll(fromRecordIndex);

			if (hasResult) {
				do {
					try {
						@SuppressWarnings("unchecked")
						E record = (E) resultScroll.get(0);
						results.add(record);
					} catch (Exception e) {
						//
					}
				} while (resultScroll.next()//
						&& resultScroll.getRowNumber() >= fromRecordIndex
						&& resultScroll.getRowNumber() < maxRecordIndex);

			}

			// The last record
			resultScroll.last();
		}

		// Total number of records
		this.totalRecords = resultScroll.getRowNumber() + 1;
		this.currentPage = pageIndex + 1;
		this.list = results;
		this.maxResult = maxResult;

		if (this.totalRecords % this.maxResult == 0) {
			this.totalPages = this.totalRecords / this.maxResult;
		} else {
			this.totalPages = (this.totalRecords / this.maxResult) + 1;
		}

		this.maxNavigationPage = maxNavigationPage;

		if (maxNavigationPage < totalPages) {
			this.maxNavigationPage = maxNavigationPage;
		}

		this.calcNavigationPages();
	}

	private void calcNavigationPages() {

		this.navigationPages = new ArrayList<Integer>();

		// First page
		navigationPages.add(1);

		int prepage = this.currentPage - 1 > 1 ? this.currentPage - 1 : 1;
		int nextpage = this.currentPage + 1 < this.totalPages ? this.currentPage + 1 : this.totalPages;

		if (prepage >= 2) {
			navigationPages.add(-1);
			navigationPages.add(prepage);
			navigationPages.add(currentPage);
		} else if (prepage == 1 && currentPage != 1) {
			navigationPages.add(currentPage);
		}

		if (nextpage < this.totalPages) {
			navigationPages.add(nextpage);
			navigationPages.add(-1);
		}

		if (currentPage != this.totalPages) {
			navigationPages.add(this.totalPages);
		}
	}

	public int getTotalPages() {
		return totalPages;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public List<E> getList() {
		return list;
	}

	public int getMaxResult() {
		return maxResult;
	}

	public List<Integer> getNavigationPages() {
		return navigationPages;
	}

}
