package com.dd.khainm.model;

import java.util.ArrayList;
import java.util.List;

/*
 * Represent both info of products and customer information
 */
public class CartInfo {

	private int orderNum;

	private CustomerInfo customerInfo;

	private List<CartLineInfo> cartLines = new ArrayList<CartLineInfo>();

	public CartInfo() {

	}
	
	public void setCartLines(ArrayList<CartLineInfo> cartLines) {
		this.cartLines = cartLines;
	}

	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public List<CartLineInfo> getCartLines() {
		return this.cartLines;
	}

	/*
	 * Get the cartLineInfo whose product's code is as the String code
	 */
	private CartLineInfo findLineByCode(String code) {
		for (CartLineInfo line : this.cartLines) {
			if (line.getProductInfo().getCode().equals(code)) {
				return line;
			}
		}
		return null;
	}

	/**
	 * add new product to the cart if the product has not been in the cart else if
	 * product has been already in the cart, update its new quantity
	 * 
	 * @param productInfo the information of the product that customer want to buy
	 * @param quantity    number of product that customer want to add
	 */
	public void addProduct(ProductInfo productInfo, int quantity) {
		CartLineInfo line = this.findLineByCode(productInfo.getCode());

		if (line == null) {
			line = new CartLineInfo();
			line.setQuantity(0);
			line.setProductInfo(productInfo);
			this.cartLines.add(line);
		}
		int newQuantity = line.getQuantity() + quantity;
		if (newQuantity <= 0) {
			this.cartLines.remove(line);
		} else {
			line.setQuantity(newQuantity);
		}
	}

	public void validate() {

	}

	/**
	 * Update the quantity of a product in the cart
	 * 
	 * @param code     to find the cartLine whose product's code is the same as the
	 *                 param
	 * @param quantity quantity of product needed to update
	 */
	public void updateProduct(String code, int quantity) {
		CartLineInfo line = this.findLineByCode(code);

		if (line != null) {
			if (quantity <= 0) {
				this.cartLines.remove(line);
			} else {
				line.setQuantity(quantity);
			}
		}
	}

	/**
	 * Remove a product from the cart often go with the condition that the number of
	 * that product is equal or less than 0
	 * 
	 * @param productInfo
	 */
	public void removeProduct(ProductInfo productInfo) {
		CartLineInfo line = this.findLineByCode(productInfo.getCode());
		if (line != null) {
			this.cartLines.remove(line);
		}
	}

	/**
	 * Check whether the cart has any products or not
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return this.cartLines.isEmpty();
	}

	/*
	 * Check customer information
	 */
	public boolean isValidCustomer() {
		return this.customerInfo != null && this.customerInfo.isValid();
	}

	/*
	 * Get quantity of products in the whole cart
	 *  
	 */
	public int getQuantityTotal() {
		int quantity = 0;
		for (CartLineInfo line : this.cartLines) {
			quantity += line.getQuantity();
		}
		return quantity;
	}
	
	/*
	 * Get the amount of money of the whole cart
	 */
	public double getAmountTotal() {
		double total = 0;
		for (CartLineInfo line : this.cartLines) {
			total += line.getAmount();
		}
		return total;
	}
	
	/*
	 * update the current cart in session from a cartform
	 */
	public void updateQuantity(CartInfo cartForm) {
		if (cartForm != null) {
			List<CartLineInfo> lines = cartForm.getCartLines();
			for (CartLineInfo line : lines) {
				this.updateProduct(line.getProductInfo().getCode(), line.getQuantity());
			}
		}

	}

}