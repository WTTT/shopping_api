package com.dd.khainm.model;

/*
 * Model is used to collect username and password that user use to login
 */
public class UserPass {
	private String username;
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserPass(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public UserPass() {
		
	}
}
