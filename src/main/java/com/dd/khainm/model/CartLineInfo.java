package com.dd.khainm.model;

/*
 * Represent a product that customer buy
 * Including the detail of the product, price of the number of this product and the number of this product 
 */
public class CartLineInfo {
	  
    private ProductInfo productInfo;
    private int quantity;
  
    public CartLineInfo() {
        this.quantity = 0;
    }
  
    public ProductInfo getProductInfo() {
        return productInfo;
    }
  
    public void setProductInfo(ProductInfo productInfo) {
        this.productInfo = productInfo;
    }
  
    public int getQuantity() {
        return quantity;
    }
  
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
  
    public double getAmount() {
        return this.productInfo.getPrice() * this.quantity;
    }
     
}
