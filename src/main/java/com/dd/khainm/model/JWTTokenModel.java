package com.dd.khainm.model;

/*
 * Represent token and authority of user
 */
public class JWTTokenModel {
	private String JWT;
	private String authority;

	public JWTTokenModel(String JWT, String authority) {
		this.JWT = JWT;
		this.authority = authority;
	}

	public String getJWT() {
		return JWT;
	}

	public void setJWT(String jWT) {
		JWT = jWT;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}
}
