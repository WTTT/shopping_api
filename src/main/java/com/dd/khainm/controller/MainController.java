package com.dd.khainm.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.dd.khainm.entity.Product;
import com.dd.khainm.form.CustomerForm;
import com.dd.khainm.model.CartInfo;
import com.dd.khainm.model.CustomerInfo;
import com.dd.khainm.model.ProductInfo;
import com.dd.khainm.pagination.PaginationResult;
import com.dd.khainm.service.OrderService;
import com.dd.khainm.service.ProductService;
import com.dd.khainm.utils.Utils;
import com.dd.khainm.validator.CustomerFormValidator;

@Controller
public class MainController {

	@Autowired
	private ProductService productService;

	@Autowired
	private CustomerFormValidator customerFormValidator;

	@Autowired
	private OrderService orderService;

	@InitBinder
	public void myInitBinder(WebDataBinder dataBinder) {

		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);

		// update quantity in shopping cart
		// (@ModelAttribute("cartForm") @Validated CartInfo cartForm)
		if (target.getClass() == CartInfo.class) {

		}

		// Customer information
		// (@ModelAttribute @Validated CustomerInfo customerForm)
		else if (target.getClass() == CustomerForm.class) {
			dataBinder.setValidator(customerFormValidator);
		}

	}

	// Direct to home page
	@RequestMapping({ "/", "/index" })
	public String index() {
		return "myindex";
	}

	// Direct to 403 page
	@RequestMapping("/403")
	public String accessDenied() {
		return "/403";
	}

	/**
	 * Product List
	 * 
	 * @param model is used to add attribute to the page, then the sent attribute
	 *              will be processed with thymeleaf
	 * @param page  is the index of the page
	 * @return is the template
	 */
	@RequestMapping({ "/productList" })
	public String listProductHandler(Model model, //
			@RequestParam(value = "page", defaultValue = "1") int page) {
		final int maxResult = 5;
		final int maxNavigationPage = 10;

		PaginationResult<ProductInfo> result = productService.queryProduct(page, //
				maxResult, maxNavigationPage);

		model.addAttribute("paginationProducts", result);
		return "productList";
	}

	/**
	 * Add a product to shopping cart
	 * 
	 * @param request is used to get request from user, in this case, to get
	 *                shopping cart of user in this session
	 * @param model   is used to add attribute to the page, then the sent attribute
	 *                will be processed with thymeleaf
	 * @param code    is the code of the product which user wants to buy
	 * @return
	 */
	@RequestMapping({ "/buyProduct" })
	public String listProductHandler(HttpServletRequest request, Model model, //
			@RequestParam(value = "code", defaultValue = "") String code) {

		Product product = null;
		if (code != null && code.length() > 0) {
			product = productService.findByCode(code);
		}
		if (product != null) {

			CartInfo cartInfo = Utils.getCartInSession(request);

			ProductInfo productInfo = new ProductInfo(product);

			cartInfo.addProduct(productInfo, 1);
		}

		return "redirect:/shoppingCart";
	}

	/**
	 * Remove a product from user's shopping cart
	 * 
	 * @param request is used to get request from user, in this case, to get
	 *                shopping cart of user in this session
	 * @param model   is used to add attribute to the page, then the sent attribute
	 *                will be processed with thymeleaf
	 * @param code    is the code of the product which user wants to buy
	 * @return
	 */
	@RequestMapping({ "/shoppingCartRemoveProduct" })
	public String removeProductHandler(HttpServletRequest request, Model model, //
			@RequestParam(value = "code", defaultValue = "") String code) {
		Product product = null;
		if (code != null && code.length() > 0) {
			product = productService.findByCode(code);
		}
		if (product != null) {

			CartInfo cartInfo = Utils.getCartInSession(request);

			ProductInfo productInfo = new ProductInfo(product);

			cartInfo.removeProduct(productInfo);

		}

		return "redirect:/shoppingCart";
	}

	/**
	 * POST: to update the quantity of products that user wants to buy
	 * 
	 * @param request  is used to get request from user, in this case, to get
	 *                 shopping cart of user in this session
	 * @param model    is used to add attribute to the page, then the sent attribute
	 *                 will be processed with thymeleaf
	 * @param cartForm get the cat form from the get request in order to update
	 *                 quantity of products
	 * @return
	 */
	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.POST)
	public String shoppingCartUpdateQty(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("cartForm") CartInfo cartForm) {

		CartInfo cartInfo = Utils.getCartInSession(request);
		cartInfo.updateQuantity(cartForm);

		return "redirect:/shoppingCart";
	}

	/**
	 * GET: Show the shopping cart, which user can update quantity, buy more
	 * products and finish shopping
	 * 
	 * @param request is used to get request from user, in this case, to get
	 *                shopping cart of user in this session
	 * @param model   is used to add attribute to the page, then the sent attribute
	 *                will be processed with thymeleaf
	 * @return
	 */
	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.GET)
	public String shoppingCartHandler(HttpServletRequest request, Model model) {
		CartInfo myCart = Utils.getCartInSession(request);

		model.addAttribute("cartForm", myCart);
		return "shoppingCart";
	}

	/**
	 * GET: Customer enter his/her info to finish their shopping, if nothing in
	 * their shopping cart, redirect to shopping cart
	 * 
	 * @param request is used to get request from user, in this case, to get
	 *                shopping cart of user in this session
	 * @param model   is used to add attribute to the page, then the sent attribute
	 *                will be processed with thymeleaf
	 * @return
	 */
	@RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.GET)
	public String shoppingCartCustomerForm(HttpServletRequest request, Model model) {

		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		}

		CustomerInfo customerInfo = cartInfo.getCustomerInfo();

		CustomerForm customerForm = new CustomerForm(customerInfo);

		model.addAttribute("customerForm", customerForm);

		return "shoppingCartCustomer";
	}

	/**
	 * POST: Process to save customer's information
	 * 
	 * @param request            is used to get request from user, in this case, to
	 *                           get shopping cart of user in this session
	 * @param model              is used to get attribute from the GET request
	 * @param customerForm       is the form that user enters information
	 * @param result             to check the customer information is valid or not
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.POST)
	public String shoppingCartCustomerSave(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("customerForm") @Validated CustomerForm customerForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			customerForm.setValid(false);
			// Go back to enter information again
			return "shoppingCartCustomer";
		}

		customerForm.setValid(true);
		CartInfo cartInfo = Utils.getCartInSession(request);
		CustomerInfo customerInfo = new CustomerInfo(customerForm);
		cartInfo.setCustomerInfo(customerInfo);

		return "redirect:/shoppingCartConfirmation";
	}

	/**
	 * GET: Check customer info again before user allows to save their information
	 * and shopping (last check)
	 * 
	 * @param request is used to get request from user, in this case, to get
	 *                shopping cart of user in this session
	 * @param model   is used to add attribute, the the sent attribute will be
	 *                proceeded by thymeleaf
	 * @return
	 */
	@RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.GET)
	public String shoppingCartConfirmationReview(HttpServletRequest request, Model model) {
		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo == null || cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		} else if (!cartInfo.isValidCustomer()) {

			return "redirect:/shoppingCartCustomer";
		}

		model.addAttribute("myCart", cartInfo);

		return "shoppingCartConfirmation";
	}

	// POST: Gửi đơn hàng (Save).
	/**
	 * POTS: Save customer information and shopping cart from GET request
	 * 
	 * @param request is used to get request from user, in this case, to get
	 *                shopping cart of user in this session
	 * @param model   is used to add attribute to the page, then the sent attribute
	 *                will be processed with thymeleaf
	 * @return
	 */
	@RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.POST)
	public String shoppingCartConfirmationSave(HttpServletRequest request, Model model) {

		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		} else if (!cartInfo.isValidCustomer()) {

			return "redirect:/shoppingCartCustomer";
		}
		try {
			orderService.saveOrder(cartInfo);
		} catch (Exception e) {
			return "/myindex";
		}

		// Remove the user shopping cart from this session
		Utils.removeCartInSession(request);

		// Save the last order in this session
		Utils.storeLastOrderedCartInSession(request, cartInfo);

		return "redirect:/shoppingCartFinalize";
	}

	/**
	 * GET: See the last order that user makes in this session
	 * 
	 * @param request is used to get request from user, in this case, to get last
	 *                order of user in this session
	 * @param model   is used to add attribute to the page, then the sent attribute
	 *                will be processed with thymeleaf
	 * @return
	 */
	@RequestMapping(value = { "/shoppingCartFinalize" }, method = RequestMethod.GET)
	public String shoppingCartFinalize(HttpServletRequest request, Model model) {

		CartInfo lastOrderedCart = Utils.getLastOrderedCartInSession(request);

		if (lastOrderedCart == null) {
			return "redirect:/shoppingCart";
		}
		model.addAttribute("lastOrderedCart", lastOrderedCart);
		return "shoppingCartFinalize";
	}

	/**
	 * GET: Get the image of requested product
	 * 
	 * @param request  to get request from user
	 * @param response send respone
	 * @param model    is used to add attribute to the page, then the sent attribute
	 *                 will be processed with thymeleaf
	 * @param code     the code of the requested product
	 * @throws IOException possible error
	 */
	@RequestMapping(value = { "/productImage" }, method = RequestMethod.GET)
	public void productImage(HttpServletRequest request, HttpServletResponse response, Model model,
			@RequestParam("code") String code) throws IOException {
		Product product = null;
		if (code != null) {
			product = this.productService.findByCode(code);
		}
		if (product != null && product.getImage() != null) {
			response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
			response.getOutputStream().write(product.getImage());
		}
		response.getOutputStream().close();
	}
}
