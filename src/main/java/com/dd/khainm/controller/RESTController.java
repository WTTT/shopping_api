package com.dd.khainm.controller;

import java.util.ArrayList;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dd.khainm.entity.Product;
import com.dd.khainm.form.CustomerForm;
import com.dd.khainm.model.CartInfo;
import com.dd.khainm.model.CartLineInfo;
import com.dd.khainm.model.CustomerInfo;
import com.dd.khainm.model.ProductInfo;
import com.dd.khainm.pagination.PaginationResult;
import com.dd.khainm.service.OrderService;
import com.dd.khainm.service.ProductService;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/api")
public class RESTController {

	@Autowired
	private ProductService productService;

	@Autowired
	private OrderService orderService;

	@RequestMapping({ "/productlist/{page}" })
	public ResponseEntity<PaginationResult<ProductInfo>> listProduct(@PathVariable(name = "page") int page) {
		final int maxResult = 6;
		final int maxNavigationPage = 10;

		PaginationResult<ProductInfo> result = productService.queryProduct(page, //
				maxResult, maxNavigationPage);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	/**
	 * POST: Customer buys a product
	 * 
	 * @param code   the provided code to find the product
	 * @param mycart customer's cart that is saved in cookies
	 * @return
	 */
	@RequestMapping(value = { "/mycart/buy/{code}" }, method = RequestMethod.POST)
	public ResponseEntity<CartInfo> listProduct(@PathVariable(name = "code") String code,
			@RequestBody(required = false) CartInfo mycart) {

		Product product = null;
		if (code != null && code.length() > 0) {
			product = productService.findByCode(code);
		}
		if (product != null) {

			if (mycart == null)
				mycart = new CartInfo();

			ProductInfo productInfo = new ProductInfo(product);

			mycart.addProduct(productInfo, 1);

			return new ResponseEntity<>(mycart, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/**
	 * POST: remove a product from customer's cart
	 * 
	 * @param mycart customer's cart that is saved in cookies
	 * @param code   the provided code to find the product
	 * @return
	 */
	@RequestMapping(value = { "/mycart/remove/{code}" }, method = RequestMethod.POST)
	public ResponseEntity<CartInfo> removeProduct(@RequestBody CartInfo mycart,
			@PathVariable(name = "code") String code) {

		Product product = null;
		if (code != null && code.length() > 0) {
			product = productService.findByCode(code);
		}
		if (product != null) {

			ProductInfo productInfo = new ProductInfo(product);

			mycart.removeProduct(productInfo);

			return new ResponseEntity<>(mycart, HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/**
	 * POST: Update the quantity of products(s) in customer's cart
	 * 
	 * @param mycart customer's cart that is saved in cookies
	 * @return
	 */
	@RequestMapping(value = { "/mycart" }, method = RequestMethod.POST)
	public ResponseEntity<CartInfo> shoppingCartUpdateQuantity(@RequestBody CartInfo mycart) {
		ArrayList<CartLineInfo> lineInfos = mycart.getCartLines().stream().filter(product -> product.getQuantity() > 0)
				.collect(Collectors.toCollection(ArrayList::new));
		mycart.setCartLines(lineInfos);
		return new ResponseEntity<>(mycart, HttpStatus.OK);
	}

	/**
	 * POST get the customerForm for customer to fill their information
	 * 
	 * @param mycart customer's cart that is saved in cookies
	 * @return
	 */
	@RequestMapping(value = { "/mycart/getcustomerform" }, method = RequestMethod.POST)
	public ResponseEntity<CustomerForm> shoppingCartCustomerForm(@RequestBody CartInfo mycart) {

		if (mycart.isEmpty()) {

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

		CustomerInfo customerInfo = mycart.getCustomerInfo();

		CustomerForm customerForm = new CustomerForm(customerInfo);

		return new ResponseEntity<>(customerForm, HttpStatus.OK);
	}

	/**
	 * POST save customerForm into their cart
	 * 
	 * @param mycart customer's cart that is saved in cookies
	 * @return
	 */
	// FIXME
	@RequestMapping(value = { "/mycart/savecustomerform" }, method = RequestMethod.POST)
	public ResponseEntity<CartInfo> shoppingCartCustomerSave(@RequestBody CartInfo mycart) {
		mycart.getCustomerInfo().setValid(true);
		return new ResponseEntity<>(mycart, HttpStatus.OK);
	}

	/**
	 * POST: Save customer's order
	 * 
	 * @param mycart customer's cart that is saved in cookies
	 * @return
	 */
	@RequestMapping(value = { "/mycart/confirmation" }, method = RequestMethod.POST)
	public ResponseEntity<?> shoppingCartConfirmationSave(@RequestBody CartInfo mycart) {

		if (mycart.isEmpty()) {

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else if (!mycart.isValidCustomer()) {

			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}
		try {
			orderService.saveOrder(mycart);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
		}
		int lastOrder = orderService.getMaxId();
		return new ResponseEntity<>(lastOrder, HttpStatus.OK);
	}
}
