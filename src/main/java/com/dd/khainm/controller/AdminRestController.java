package com.dd.khainm.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dd.khainm.entity.Product;
import com.dd.khainm.form.ProductForm;
import com.dd.khainm.model.JWTTokenModel;
import com.dd.khainm.model.OrderDetailInfo;
import com.dd.khainm.model.OrderInfo;
import com.dd.khainm.model.ProductInfo;
import com.dd.khainm.model.UserPass;
import com.dd.khainm.pagination.PaginationResult;
import com.dd.khainm.service.AccountService;
import com.dd.khainm.service.OrderService;
import com.dd.khainm.service.ProductService;
import com.dd.khainm.service.TokenAuthenticationService;

@CrossOrigin
@RestController
@RequestMapping("/api/admin")
public class AdminRestController {

	@Autowired
	AccountService accountService;

	@Autowired
	TokenAuthenticationService tokenAuthenticationService;

	@Autowired
	ProductService productService;

	@Autowired
	OrderService orderService;

	@Autowired
	AuthenticationManager authenticationManager;

	/**
	 * Get the token for the username and password that user send
	 * 
	 * @param info contains username and password that user send
	 * @return
	 */
	@RequestMapping("/gettoken")
	public ResponseEntity<?> authenticateAdmin(@RequestBody UserPass info) {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(info.getUsername(), info.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);

		UserDetails userDetails = accountService.loadUserByUsername(info.getUsername());

		String[] authority = new String[] { "" };

		userDetails.getAuthorities().forEach(auth -> {
			authority[0] += auth.toString();
		});

		if (authentication != null) {
			return new ResponseEntity<>(new JWTTokenModel(
					TokenAuthenticationService.getToken(info.getUsername(), userDetails.getAuthorities()),
					authority[0]), HttpStatus.OK);
		}

		return null;
	}

	/**
	 * Get the basic information of user
	 * 
	 * @param req to get the jwt
	 * @return
	 */
	@RequestMapping("/accountinfo")
	public ResponseEntity<UserDetails> getUserDetails(HttpServletRequest req) {
		return new ResponseEntity<>(tokenAuthenticationService.getUserFromJWT(req), HttpStatus.OK);
	}

	/**
	 * Get productlist according to page
	 * 
	 * @param page index page
	 * @return
	 */
	@RequestMapping({ "/productlist/{page}" })
	public ResponseEntity<PaginationResult<ProductInfo>> listProduct(@PathVariable(name = "page") int page) {
		final int maxResult = 6;
		final int maxNavigationPage = 10;

		PaginationResult<ProductInfo> result = productService.queryProduct(page, maxResult, maxNavigationPage);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	/**
	 * Get order list according to page
	 * 
	 * @param pageStr page index
	 * @return
	 */
	@RequestMapping(value = { "/orderlist/{page}" })
	public ResponseEntity<PaginationResult<OrderInfo>> orderList(@PathVariable(name = "page") String pageStr) {
		int page = 1;
		page = Integer.parseInt(pageStr);
		final int MAX_RESULT = 6;
		final int MAX_NAVIGATION_PAGE = 10;

		PaginationResult<OrderInfo> paginationResult //
				= orderService.listOrderInfo(page, MAX_RESULT, MAX_NAVIGATION_PAGE);

		return new ResponseEntity<>(paginationResult, HttpStatus.OK);
	}

	/**
	 * Get the order information by provided id
	 * 
	 * @param orderId provided id
	 * @return
	 */
	@RequestMapping(value = { "/orderlist/order/{orderid}" })
	public ResponseEntity<OrderInfo> orderView(@PathVariable(name = "orderid") int orderId) {

		OrderInfo orderInfo = null;
		if (orderId >= 0) {
			orderInfo = this.orderService.getOrderInfo(orderId);
		}
		if (orderInfo == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

		List<OrderDetailInfo> details = this.orderService.listOrderDetailInfos(orderId);
		orderInfo.setDetails(details);
		return new ResponseEntity<>(orderInfo, HttpStatus.OK);
	}

	/**
	 * GET a new ProductForm to add a new product
	 * 
	 * @return
	 */
	@RequestMapping(value = "/productlist/add", method = RequestMethod.GET)
	public ResponseEntity<ProductForm> addnew() {
		return new ResponseEntity<>(new ProductForm(), HttpStatus.OK);
	}

	/**
	 * POST Create a new product
	 * 
	 * @param file       image for the new product (if exists)
	 * @param name       name for the new product
	 * @param price      price for the new product
	 * @param code       code for the new product
	 * @param newProduct check if this product is a new ones or not
	 * @return
	 */
	@RequestMapping(value = { "/productlist/add" }, method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<?> productSave(@RequestParam(name = "fileData", required = false) MultipartFile file,
			@RequestParam("name") String name, @RequestParam("price") double price, @RequestParam("code") String code,
			@RequestParam("newProduct") boolean newProduct) {

		if (productService.findByCode(code) != null)
			return new ResponseEntity<>("Duplicated code!", HttpStatus.ALREADY_REPORTED);

		ProductForm productForm = new ProductForm();

		productForm.setCode(code);
		productForm.setName(name);
		productForm.setFileData(file);
		productForm.setPrice(price);
		productForm.setNewProduct(newProduct);

		try {
			productService.saveByForm(productForm);
		} catch (Exception e) {
			System.out.println("Error");
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/**
	 * GET Get the productForm to edit existed product
	 * 
	 * @param code to find the product that user want to edit
	 * @return
	 */
	@RequestMapping(value = "/productlist/product/{code}", method = RequestMethod.GET)
	public ResponseEntity<ProductForm> getProduct(@PathVariable(name = "code") String code) {

		Product product = productService.findByCode(code);
		ProductForm productForm = new ProductForm();

		productForm.setCode(code);
		productForm.setName(product.getName());
		productForm.setPrice(product.getPrice());
		productForm.setFileData(null);
		productForm.setNewProduct(false);

		return new ResponseEntity<>(productForm, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = { "/productlist/product" }, method = RequestMethod.GET, consumes = "multipart/form-data")
	public void editProductSave(){
	}
	
	/**
	 * POST edit the product
	 * 
	 * @param file	new (or not) image for product
	 * @param name 	new (or not) name for product
	 * @param price	new (or not) price for product	
	 * @param code	code of product
	 * @param newProduct	
	 * @return
	 */
	@RequestMapping(value = { "/productlist/product" }, method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<?> editProductSave(@RequestParam(name = "fileData", required = false) MultipartFile file,
			@RequestParam("name") String name, @RequestParam("price") double price, @RequestParam("code") String code,
			@RequestParam("newProduct") boolean newProduct) {

		ProductForm productForm = new ProductForm();

		productForm.setCode(code);
		productForm.setName(name);
		productForm.setFileData(file);
		productForm.setPrice(price);
		productForm.setNewProduct(newProduct);

		try {
			productService.saveByForm(productForm);
		} catch (Exception e) {
			System.out.println("Error");
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
