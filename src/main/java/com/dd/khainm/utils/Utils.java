package com.dd.khainm.utils;

import javax.servlet.http.HttpServletRequest;

import com.dd.khainm.model.CartInfo;
 
public class Utils {
	
	/*
	 * Get the cart in session
	 */
    public static CartInfo getCartInSession(HttpServletRequest request) {
  
        CartInfo cartInfo = (CartInfo) request.getSession().getAttribute("myCart");
   
        if (cartInfo == null) {
            cartInfo = new CartInfo(); 
             
            request.getSession().setAttribute("myCart", cartInfo);
        }
 
        return cartInfo;
    }
    
    /*
     * Set the provided cart as the current cart
     */
    public static void setCartInSession(HttpServletRequest request, CartInfo cartInfo) {
    	request.getSession().setAttribute("myCart", cartInfo);
    }
    
    /*
     * Remove cart in session
     */
    public static void removeCartInSession(HttpServletRequest request) {
        request.getSession().removeAttribute("myCart");
    }
    
    /*
     * set the last order in session
     */
    public static void storeLastOrderedCartInSession(HttpServletRequest request, CartInfo cartInfo) {
        request.getSession().setAttribute("lastOrderedCart", cartInfo);
    }
    
    /*
     * Get the last order in session
     */
    public static CartInfo getLastOrderedCartInSession(HttpServletRequest request) {
        return (CartInfo) request.getSession().getAttribute("lastOrderedCart");
    }
      
}
