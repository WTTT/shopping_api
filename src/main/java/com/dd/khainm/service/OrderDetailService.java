package com.dd.khainm.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dd.khainm.entity.OrderDetail;
import com.dd.khainm.repository.OrderDetailRepository;

@Service
public class OrderDetailService {

	@Autowired
	OrderDetailRepository orderDetailRepository;
	
	public OrderDetail findById(int id) {
		
		List<OrderDetail> list = orderDetailRepository.findById(id);
		
		if(list.size() == 0) return null;
		else return list.get(0);
		
	}
	
	public void save(OrderDetail orderDetail) {
		orderDetailRepository.save(orderDetail);
	}
	
	public OrderDetail delete(int id) {
		
		OrderDetail orderDetail = orderDetailRepository.findById(id).get(0);
		
		orderDetail.setDeleted(true);
		orderDetail.setDeletedAt(new Date());
		
		this.save(orderDetail);
		
		return orderDetail;
	}
	
	public int getMaxId() {
		
		int maxId= 0;
		List<OrderDetail> list = orderDetailRepository.findAll();
		if (list.size() != 0) {
			maxId = list.get(list.size() - 1).getId();
		}

		return maxId;
	}
}
