package com.dd.khainm.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dd.khainm.entity.Account;
import com.dd.khainm.repository.AccountRepository;

@Service
public class AccountService implements UserDetailsService{

	@Autowired
	AccountRepository accountRepository;
	
	public Account findById(int id) {
		
		List<Account> list = accountRepository.findById(id);
		
		if(list.size() == 0) return null;
		else return list.get(0);
		
	}
	
	public Account findByName(String name) {
		
		List<Account> list = accountRepository.findById(name);
		
		if(list.size() == 0) return null;
		else return list.get(0);
	}

	@Override
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
		
		Account account = this.findByName(name);
		
		if (account == null) {
            throw new UsernameNotFoundException("User " //
                    + name + " was not found in the database");
        }
		
		String role = account.getUserRole();
		
		List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
		       
        GrantedAuthority authority = new SimpleGrantedAuthority(role);
 
        grantList.add(authority);
 
        boolean enabled = account.isActive();
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
 
        UserDetails userDetails = (UserDetails) new User(account.getUserName(), //
                account.getEncrytedPassword(), enabled, accountNonExpired, //
                credentialsNonExpired, accountNonLocked, grantList);
 
        return userDetails;
	}
	
}
