package com.dd.khainm.service;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenAuthenticationService {
	static final long EXPIRATIONTIME = 864_000_000; // 10 days
	static final String SECRET = "ThisIsASecret";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";
	
	@Autowired
	private AccountService accountService;
	
	public static void addAuthentication(HttpServletResponse res, String username,
			Collection<? extends GrantedAuthority> authorities) {

		ArrayList<String> authsList = new ArrayList<>(authorities.size());

		for (GrantedAuthority authority : authorities) {
			authsList.add(authority.getAuthority());
		}

		String JWT = Jwts.builder().setSubject(username).claim("roles", authsList)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
				.signWith(SignatureAlgorithm.HS512, SECRET).compact();
		res.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
	}

	public static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		if (token != null) {
			// parse the token.
			String user = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody()
					.getSubject();

			Collection<? extends GrantedAuthority> authorities = Arrays
					.asList(Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
							.getBody().get("roles", Collection.class).toString().replace("[","").replace("]","").split(","))
					.stream().map(authority -> new SimpleGrantedAuthority(authority)).collect(Collectors.toList());
			
			System.out.println(authorities.toArray()[0].getClass().getName());
			return user != null ? new UsernamePasswordAuthenticationToken(user, null, authorities) : null;
		}
		return null;
	}
	
	public UserDetails getUserFromJWT(HttpServletRequest req) {
		String token = req.getHeader(HEADER_STRING);
		if(token != null) {
			String user = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody()
					.getSubject();
			return accountService.loadUserByUsername(user);
		}
		return null;
	}
	
	public static String getToken(String username,
			Collection<? extends GrantedAuthority> authorities) {

		ArrayList<String> authsList = new ArrayList<>(authorities.size());

		for (GrantedAuthority authority : authorities) {
			authsList.add(authority.getAuthority());
		}

		String JWT = Jwts.builder().setSubject(username).claim("roles", authsList)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
				.signWith(SignatureAlgorithm.HS512, SECRET).compact();
		return JWT;
	}
}
